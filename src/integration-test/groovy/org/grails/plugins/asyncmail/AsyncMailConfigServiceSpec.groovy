package org.grails.plugins.asyncmail

import grails.testing.mixin.integration.Integration
import spock.lang.Specification

/**
 * Test default configuration
 */
@Integration
class AsyncMailConfigServiceSpec extends Specification {
    AsyncMailConfigService asyncMailConfigService

    void "test configuration"() {
        expect: "the service returns defult values"
            asyncMailConfigService.defaultAttemptInterval == 300000l
            asyncMailConfigService.defaultMaxAttemptCount == 1
            asyncMailConfigService.sendRepeatInterval == 60000l
            asyncMailConfigService.expiredCollectorRepeatInterval == 607000l
            asyncMailConfigService.messagesAtOnce == 100
            asyncMailConfigService.sendImmediately
            !asyncMailConfigService.clearAfterSent
            !asyncMailConfigService.disable
            asyncMailConfigService.useFlushOnSave
            asyncMailConfigService.persistenceProvider == 'hibernate4'
            !asyncMailConfigService.newSessionOnImmediateSend
            asyncMailConfigService.taskPoolSize == 1
            !asyncMailConfigService.mongo
    }
}
