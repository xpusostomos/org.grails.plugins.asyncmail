package org.grails.plugins.asyncmail

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import org.grails.plugins.asyncmail.enums.MessageStatus
import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import org.springframework.transaction.TransactionDefinition
import spock.lang.Specification
import spock.lang.Unroll

import static org.grails.plugins.asyncmail.enums.MessageStatus.SENT

/**
 * Integration tests for process service
 */
@Integration
@Rollback
class AsyncMailProcessServiceIntegrationSpec extends Specification implements GrailsConfigurationAware {
    AsyncMailProcessService asyncMailProcessService
    Config configuration

    void setup() {
        asyncMailProcessService.asyncMailSendService = Mock(AsyncMailSendService)
    }

    void cleanup() {
        AsyncMailMessage.list()*.delete()
        configuration.asynchronous.mail.taskPoolSize = 1
        configuration.asynchronous.mail.useFlushOnSave = true
    }

    @Unroll
    void "test process #messageCount messages with #taskCount tasks and flush=#flush"(
            int messageCount, int taskCount, boolean flush
    ) {
        given: "some messages"
            AsyncMailMessage.withTransaction(
                    propagationBehavior: TransactionDefinition.PROPAGATION_REQUIRES_NEW
            ) {
                for (int i = 0; i < messageCount; i++) {
                    new AsyncMailMessage(
                            from: 'john.smith@example.com',
                            to: ['jane.smith@example.con'],
                            subject: 'Subject',
                            text: 'Body'
                    ).save(flush: true, failonError: true)
                }
            }

            configuration.asynchronous.mail.taskPoolSize = taskCount
            configuration.asynchronous.mail.useFlushOnSave = flush
        when: "process"
            asyncMailProcessService.findAndSendEmails()
        then: "all messages have status SENT"
            messageCount * asyncMailProcessService.asyncMailSendService.send(_ as AsyncMailMessage)
            AsyncMailMessage.countByStatus(SENT) == messageCount
            AsyncMailMessage.countByStatusNotEqual(SENT) == 0
        where:
            messageCount | taskCount | flush
            1            | 1         | false
            1            | 10        | false
            10           | 1         | false
            9            | 10        | false
            10           | 10        | false
            11           | 10        | false
            1            | 1         | true
            1            | 10        | true
            10           | 1         | true
            9            | 10        | true
            10           | 10        | true
            11           | 10        | true
    }

    void "test delete messages after sent"() {
        given: "some meeesage with mark delete true"
            AsyncMailMessage.withTransaction(
                    propagationBehavior: TransactionDefinition.PROPAGATION_REQUIRES_NEW
            ) {
                for (int i = 0; i < 5; i++) {
                    new AsyncMailMessage(
                            from: 'john.smith@example.com',
                            to: ['jane.smith@example.con'],
                            subject: 'Subject',
                            text: 'Body',
                            markDelete: true
                    ).save(flush: true, failonError: true)
                }
            }
        when: "process them"
            asyncMailProcessService.findAndSendEmails()
        then: "all have been deleted"
            5 * asyncMailProcessService.asyncMailSendService.send(_ as AsyncMailMessage)
            AsyncMailMessage.count() == 0
    }

    void "test delet attachments"() {
        given: "some meeesage with mark delete true"
            AsyncMailMessage.withTransaction(
                    propagationBehavior: TransactionDefinition.PROPAGATION_REQUIRES_NEW
            ) {
                for (int i = 0; i < 5; i++) {
                    new AsyncMailMessage(
                            from: 'john.smith@example.com',
                            to: ['jane.smith@example.con'],
                            subject: 'Subject',
                            text: 'Body',
                            markDeleteAttachments: true,
                            attachments: [
                                    new AsyncMailAttachment(
                                            attachmentName: "Attachment $i",
                                            content: '' as byte[]
                                    )
                            ]
                    ).save(flush: true, failonError: true)
                }
            }
        when: "process them"
            asyncMailProcessService.findAndSendEmails()
        then: "all have been deleted"
            5 * asyncMailProcessService.asyncMailSendService.send(_ as AsyncMailMessage)
            AsyncMailMessage.count() == 5
            AsyncMailAttachment.count() == 0
    }

    void "test process error message"() {
        given: "A message with an error in DB"
            AsyncMailMessage.withTransaction(
                    propagationBehavior: TransactionDefinition.PROPAGATION_REQUIRES_NEW
            ) {
                new AsyncMailMessage(
                        from: 'john.smith@example.com',
                        subject: 'Subject',
                        text: 'Body'
                ).save(flush: true, failonError: true, validate: false)
            }
        when: "process"
            asyncMailProcessService.findAndSendEmails()
        then: "it's status is error"
            AsyncMailMessage.list()[0].status == MessageStatus.ERROR
    }
}
