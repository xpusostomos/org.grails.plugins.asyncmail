package org.grails.plugins.asyncmail

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

import static org.grails.plugins.asyncmail.enums.MessageStatus.EXPIRED

/**
 * @author Vitalii Samolovskikh aka Kefir, Puneet Behl
 */
@Integration
@Rollback
class AsyncMailPersistenceServiceSpec extends Specification {

    @Autowired
    AsyncMailPersistenceService asyncMailPersistenceService

    void testCycle() {
        given: "a message"
            AsyncMailMessage message = new AsyncMailMessage(
                    from: 'John Smith <john@example.com>',
                    to: ['Mary Smith <mary@example.com>'],
                    subject: 'Subject',
                    text: 'Text'
            )
        when: 'a message is instantiated and saved'
            //message.save flush
            asyncMailPersistenceService.save(message, true, true)
            List<Long> ids = asyncMailPersistenceService.selectMessagesIdsForSend()

        then: 'selectMessagesIdsForSend should return list with 1 messageId'
            1 == ids.size()
            ids[0] == message.id

        when: "get message"
            AsyncMailMessage message1 = asyncMailPersistenceService.getMessage(ids[0])

        then: "message1 id message"
            message1.id == message.id
            message1.from == 'John Smith <john@example.com>'
            message1.to == ['Mary Smith <mary@example.com>']
            message1.subject == 'Subject'
            message1.text == 'Text'

        when: 'deleted the message'
            asyncMailPersistenceService.delete(message1, true)

        then: 'message to be send count should be 0'
            0 == asyncMailPersistenceService.selectMessagesIdsForSend()?.size()
    }

    void testDeleteAttachments() {
        given: "a message"
            AsyncMailMessage message = new AsyncMailMessage(
                    from: 'John Smith <john@example.com>',
                    to: ['Mary Smith <mary@example.com>'],
                    subject: 'Subject',
                    text: 'Text',
                    attachments: [
                            new AsyncMailAttachment(
                                    attachmentName: 'name',
                                    content: 'Grails'.getBytes()
                            )
                    ]
            )
        when: 'a message is instantiated and saved'

            asyncMailPersistenceService.save(message, true, true)
            List<Long> ids = asyncMailPersistenceService.selectMessagesIdsForSend()

        then: 'selectMessagesIdsForSend should return list with 1 messageId'
            1 == ids.size()

        when: "get message"
            AsyncMailMessage message1 = asyncMailPersistenceService.getMessage(ids[0])

        then: "it contains attachments"
            message1.attachments.size() == 1

        when: 'deleted the message'
            asyncMailPersistenceService.deleteAttachments(message, true)
            message1 = asyncMailPersistenceService.getMessage(ids[0])

        then: 'the message should have no attachments'
            message1.attachments ? false : true
    }

    void testSaveSimpleMessage() {
        setup:
            AsyncMailMessage message = new AsyncMailMessage(
                    bcc: ['mary@example.com'],
                    subject: 'Subject',
                    text: 'Text'
            )

        when:
            asyncMailPersistenceService.save(message, true, true)
        then:
            AsyncMailMessage dbMessage = asyncMailPersistenceService.getMessage(message.id)
            dbMessage.bcc == ['mary@example.com']

        cleanup:
            asyncMailPersistenceService.delete(message, true)
    }

    void testUpdateExpiredMessages() {
        when: 'message is saved with expired endDate'
            AsyncMailMessage message = new AsyncMailMessage(
                    from: 'John Smith <john@example.com>',
                    to: ['Mary Smith <mary@example.com>'],
                    subject: 'Subject',
                    text: 'Text',
                    beginDate: new Date(System.currentTimeMillis() - 2),
                    endDate: new Date(System.currentTimeMillis() - 1)
            )
            asyncMailPersistenceService.save message, true, true

        then: 'selectMessagesIdsForSend should return empty list'
            0 == asyncMailPersistenceService.selectMessagesIdsForSend()?.size()

        when:
            asyncMailPersistenceService.updateExpiredMessages()
            message.refresh()

        then:
            EXPIRED == message.status
    }
}
