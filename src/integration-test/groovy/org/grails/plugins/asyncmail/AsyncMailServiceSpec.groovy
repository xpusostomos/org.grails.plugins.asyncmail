package org.grails.plugins.asyncmail

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

import static org.grails.plugins.asyncmail.enums.MessageStatus.CREATED

/**
 * @author Vitalii Samolovskikh aka Kefir, Puneet Behl
 */
@Integration
@Rollback
class AsyncMailServiceSpec extends Specification {

    public static final String VALUE_MAIL = 'test@example.com'

    @Autowired
    AsyncMailService asyncMailService

    void testSendAsyncMail() {
        when:
            asyncMailService.sendMail {
                to VALUE_MAIL
                subject 'Test'
                text 'Test'
                immediate false
            }
            AsyncMailMessage message = AsyncMailMessage.findAll()[0]

        then:
            VALUE_MAIL == message.to[0]
            CREATED == message.status
    }
}
