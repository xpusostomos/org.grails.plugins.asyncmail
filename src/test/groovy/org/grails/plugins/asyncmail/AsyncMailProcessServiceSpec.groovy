package org.grails.plugins.asyncmail

import grails.testing.gorm.DataTest
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

import static org.grails.plugins.asyncmail.enums.MessageStatus.SENT

/**
 * @author Vitalii Samolovskikh aka Kefir, Puneet Behl
 */
class AsyncMailProcessServiceSpec extends Specification implements ServiceUnitTest<AsyncMailProcessService>, DataTest {

    AsyncMailMessage asyncMailPersistenceService
    AsyncMailSendService asyncMailSendService
    AsyncMailConfigService asyncMailConfigService

    void setupSpec(){
        mockDomain AsyncMailMessage
    }

    void setup() {
        asyncMailSendService = Mock(AsyncMailSendService)
        asyncMailPersistenceService = Stub(AsyncMailMessage) {
            save(_, _, _) >> { AsyncMailMessage message, boolean flush, boolean validate ->
                message.save(flush: flush, validata: validate)
            }

            getMessage(_) >> { long id ->
                AsyncMailMessage.get(id)
            }
        }
        asyncMailConfigService = Stub(AsyncMailConfigService) {
            getTaskPoolSize() >> 1
            isUseFlushOnSave() >> true
        }

        service.asyncMailConfigService = asyncMailConfigService
        service.asyncMailPersistenceService = asyncMailPersistenceService
        service.asyncMailSendService = asyncMailSendService
    }

    void testProcessEmail() {
        setup:
            AsyncMailMessage message = new AsyncMailMessage(
                    from: 'John Smith <john@example.com>',
                    to: ['Mary Smith <mary@example.com>'],
                    subject: 'Subject',
                    text: 'Text'
            )
            asyncMailPersistenceService.save message, true, true

        when:
            service.processEmailMessage(message.id)

        then:
            1 * asyncMailSendService.send(_)
            message.status == SENT
            message.sentDate
    }
}

