package org.grails.plugins.asyncmail

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

import static AsyncMailAttachment.DEFAULT_MIME_TYPE

/**
 * @author Vitalii Samolovskikh aka Kefir, Puneet Behl
 */
class AsyncMailAttachmentSpec extends Specification implements DomainUnitTest<AsyncMailAttachment> {


    void "testing default constructor"() {
        when:
        AsyncMailAttachment attachment = new AsyncMailAttachment()

        then:
        !attachment.attachmentName
        attachment.mimeType == DEFAULT_MIME_TYPE
        !attachment.content
        !attachment.inline
    }

    void "testing constraints"() {
        setup:
        AsyncMailAttachment attachment

        when: 'constraints on default attachment'
        attachment = new AsyncMailAttachment()

        then: 'should fail validation'
        !attachment.validate()
        attachment.errors.getFieldError('attachmentName').code == 'nullable'
        attachment.errors.getFieldError('content').code == 'nullable'
        attachment.errors.getFieldError('message').code == 'nullable'

        when: 'attachmentName is empty'
        attachment = new AsyncMailAttachment(attachmentName: '')

        then: 'should fail validation with nullable error on attachmentName'
        !attachment.validate()
        attachment.errors.getFieldError('attachmentName').code == 'nullable'

        when: 'mimeType is null'
        attachment = new AsyncMailAttachment(mimeType: null)

        then: 'should fail validation on mimeType'
        !attachment.validate()
        attachment.errors.getFieldError('mimeType').code == 'nullable'

        when: 'a valid attachment'
        attachment =  new AsyncMailAttachment(
                attachmentName:'name',
                content:'Grails'.getBytes(),
                message: new AsyncMailMessage()
        )

        then: 'should pass all validations'
        attachment.validate()

    }
}
