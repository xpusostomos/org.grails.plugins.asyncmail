package org.grails.plugins.asyncmail

import grails.plugins.mail.MailMessageContentRender
import grails.testing.gorm.DataTest
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.mop.ConfineMetaClassChanges

import static org.grails.plugins.asyncmail.enums.MessageStatus.CREATED

/**
 * @author Vitalii Samolovskikh aka Kefir, Puneet Behl
 */
@ConfineMetaClassChanges(AsyncMailMessageBuilder)
class AsyncMailMessageBuilderSpec extends Specification implements DataTest {

    AsyncMailMessageBuilderFactory asyncMailMessageBuilderFactory

    void setupSpec() {
        mockDomain AsyncMailMessage
    }

    void setup() {
        asyncMailMessageBuilderFactory = new AsyncMailMessageBuilderFactory()
        asyncMailMessageBuilderFactory.configuration = grailsApplication.config
    }

    @Unroll("testing builder for params immediate = #immediateVal and delete = #deleteVal")
    void "testing builder"() {
        setup:
        def c = {
            from 'John Smith <john@example.com>'
            to 'test1@example.com'
            envelopeFrom 'mary@example.com'
            subject 'Subject'
            text 'Text'
            immediate immediateVal
            delete deleteVal
            priority 1
        }
        AsyncMailMessageBuilder builder
        AsyncMailMessage message

        when:
        builder = asyncMailMessageBuilderFactory.createBuilder()
        c.delegate = builder
        c.call()
        message = builder.message
        message.validate()

        then:
        message.from == 'John Smith <john@example.com>'
        message.to == ['test1@example.com']
        message.envelopeFrom == 'mary@example.com'
        message.subject == 'Subject'
        message.text == 'Text'
        message.status == CREATED
        message.markDelete == markDeleteVal
        message.markDeleteAttachments == markDeleteAttachmentsVal

        message.priority == 1
        builder.immediatelySetted == immediatelySettedVal
        builder.immediately == immediatelyVal

        where:

        immediateVal | deleteVal     | immediatelySettedVal | immediatelyVal | markDeleteVal | markDeleteAttachmentsVal
        true         | true          |  true                | true           | true          | false
        false        | 'attachments' |  true                | false          | false         | true
        true         | false         |  true                | true           | false         | false
    }

    void "testing mail with minimum data"() {
        setup:
        def c = {
            to 'john@example.com'
            subject 'Subject'
            text 'Text'
        }
        AsyncMailMessageBuilder builder
        AsyncMailMessage message

        when:
        builder = asyncMailMessageBuilderFactory.createBuilder()
        c.delegate = builder
        c.call()
        message = builder.message

        then:
        message.validate()
    }

    void testBodyHtmlRender(){
        setup:
        overrideDoRenderMethod('text/html')
        AsyncMailMessageBuilder builder
        AsyncMailMessage message
        def c = {
            to 'test@example.com'
            subject 'Subject'
            body view:'/test/html'
        }

        when:
        builder = asyncMailMessageBuilderFactory.createBuilder()
        c.delegate = builder
        c.call()
        message = builder.message
        message.validate()

        then:
        message.to == ['test@example.com']
        message.subject == 'Subject'
        message.html
        message.text
    }

    void testHtmlRender(){
        setup:
        overrideDoRenderMethod('text/html')
        AsyncMailMessageBuilder builder
        AsyncMailMessage message
        def c = {
            to 'test@example.com'
            subject 'Subject'
            locale Locale.ENGLISH
            html view:'/test/html'
        }

        when:
        builder = asyncMailMessageBuilderFactory.createBuilder()
        c.delegate = builder
        c.call()
        message = builder.message
        message.validate()

        then:
        message.to == ['test@example.com']
        message.subject == 'Subject'
        message.html
        message.text
    }

    void testTextRender(){
        setup:
        overrideDoRenderMethod('text/plain')
        AsyncMailMessageBuilder builder
        AsyncMailMessage message
        def c = {
            to 'test@example.com'
            subject 'Subject'
            locale 'en_us'
            text view:'/test/plain'
        }

        when:
        builder = asyncMailMessageBuilderFactory.createBuilder()
        c.delegate = builder
        c.call()
        message = builder.message
        message.validate()

        then:
        message.to == ['test@example.com']
        message.subject == 'Subject'
        !message.html
        message.text
    }

    void testBodyTextRender(){
        setup:
        overrideDoRenderMethod('text/plain')
        AsyncMailMessageBuilder builder
        AsyncMailMessage message
        def c = {
            to 'test@example.com'
            subject 'Subject'
            body view:'/test/plain'
        }

        when:
        builder = asyncMailMessageBuilderFactory.createBuilder()
        c.delegate = builder
        c.call()
        message = builder.message
        message.validate()

        then:
        message.to == ['test@example.com']
        message.subject == 'Subject'
        !message.html
        message.text
    }

    void testMultipartAlternative(){
        setup:
            AsyncMailMessageBuilder builder
            AsyncMailMessage message
            def c = {
                to 'test@example.com'
                subject 'Subject'
                html '<html>HTML text</html>'
                text 'Plain text'
            }

        when:
            builder = asyncMailMessageBuilderFactory.createBuilder()
            c.delegate = builder
            c.call()
            message = builder.message
            message.validate()

        then:
            message.to == ['test@example.com']
            message.subject == 'Subject'
            message.html
            message.text
            message.alternative
    }

    protected void overrideDoRenderMethod(String contentType) {
        AsyncMailMessageBuilder.metaClass.doRender = {Map params->
            new MailMessageContentRender(null, contentType)
        }
    }
}
