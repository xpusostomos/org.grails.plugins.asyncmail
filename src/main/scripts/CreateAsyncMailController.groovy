import org.grails.io.support.SpringIOUtils

description("Creates a new Asynchronous Mail Controller for managing asynchronous messages.") {
    usage "grails create-async-mail-controller [CONTROLLER NAME]"
//    argument name: 'Controller Name', description: "The name of the controller"
}

def m
if (args[0]) {
    m = model(args[0])
} else {
    m = model('org.grails.plugins.asyncmail.AsyncMail')
}

render template: "artifacts/AsyncMailController.groovy",
        destination: file("grails-app/controllers/$m.packagePath/${m.convention('Controller')}.groovy"),
        model: m

//files("src/main/templates/scaffolding/asyncmail/*.gsp").each {
//    copy $it "grails-app/views"
//}
//copy {
//    from templates("scaffolding/asyncmail/*.gsp")
//    into "grails-app/views"
//}
//
//templates("scaffolding/**.gsp").each { r ->
//    String path = r.URL.toString().replaceAll(/^.*?META-INF/, "grails-app/views")
//    File fpath = new File(path)
////    if (path.endsWith('/')) {
////        mkdir(path)
////    } else {
////        File to = new File(path)
//        File to = new File("grails-app/views", fpath.name)
//        SpringIOUtils.copy(r, to)
//        println("Copied ${r.filename} to location ${to.canonicalPath}")
////    }
//}

//templates("scaffolding/asyncmail/*.gsp").each {
//templates("scaffolding/**").each {
//    System.out.println it.URL.toString()
//}


