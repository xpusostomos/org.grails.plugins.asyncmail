package org.grails.plugins.asyncmail

import grails.plugins.Plugin
import grails.plugins.quartz.JobDescriptor
import grails.plugins.quartz.JobManagerService
import grails.plugins.quartz.TriggerDescriptor
import groovy.util.logging.Slf4j
import org.quartz.Scheduler
import org.quartz.TriggerKey
import grails.config.Config

@Slf4j
class AsyncMailGrailsPlugin extends Plugin {
    def grailsVersion = "3.3.0 > *"
    def loadAfter = ['mail', 'quartz', 'hibernate', 'hibernate3', 'hibernate4', 'hibernate5', 'mongodb']

    Closure doWithSpring() { { ->
            asyncMailMessageBuilderFactory(AsyncMailMessageBuilderFactory) {
                it.autowire = true
            }

            springConfig.addAlias 'asyncMailService', 'asyncMailService'
        }
    }

    @Override
    void onStartup(Map<String, Object> event) {
        // Starts jobs
        startJobs(applicationContext)
    }

    /**
     * Start the send job and the messages collector.
     *
     * If the plugin is used in cluster we have to remove old triggers.
     */
    def startJobs(applicationContext) {
        Config asyncMailConfig = grailsApplication.config
                //.getProperty('asynchronous.mail')
        if (!asyncMailConfig.getProperty('asynchronous.mail.disable')) {
            JobManagerService jobManagerService = applicationContext.jobManagerService
            Scheduler quartzScheduler = applicationContext.quartzScheduler

            // Get our jobs
            List<JobDescriptor> jobDescriptors = jobManagerService.getJobs("AsyncMail")

            // Remove old triggers for the send job
            log.debug("Removing old triggers for the AsyncMailJob")
            JobDescriptor sjd = jobDescriptors.find { it.name == 'grails.plugin.asyncmail.AsyncMailJob' }
            sjd?.triggerDescriptors?.each {TriggerDescriptor td ->
                def triggerKey = new TriggerKey(td.name, td.group)
                quartzScheduler.unscheduleJob(triggerKey)
                log.debug("Removed the trigger ${triggerKey} for the AsyncMailJob")
            }

            // Schedule the send job
            def sendInterval = (Long) asyncMailConfig.getProperty('asynchronous.mail.send.repeat.interval')
            log.debug("Scheduling the AsyncMailJob with repeat interval ${sendInterval}ms")
            AsyncMailJob.schedule(sendInterval)
            log.debug("Scheduled the AsyncMailJob with repeat interval ${sendInterval}ms")

            // Remove old triggers for the collector job
            log.debug("Removing old triggers for the ExpiredMessagesCollectorJob")
            JobDescriptor cjd = jobDescriptors.find { it.name == 'grails.plugin.asyncmail.ExpiredMessagesCollectorJob' }
            cjd?.triggerDescriptors?.each {TriggerDescriptor td ->
                def triggerKey = new TriggerKey(td.name, td.group)
                quartzScheduler.unscheduleJob(triggerKey)
                log.debug("Removed the trigger ${triggerKey} for the ExpiredMessagesCollectorJob")
            }

            // Schedule the collector job
            def collectInterval = (Long) asyncMailConfig.getProperty('asynchronous.mail.expired.collector.repeat.interval')
            log.debug("Scheduling the ExpiredMessagesCollectorJob with repeat interval ${collectInterval}ms")
            ExpiredMessagesCollectorJob.schedule(collectInterval)
            log.debug("Scheduled the ExpiredMessagesCollectorJob with repeat interval ${collectInterval}ms")
        }
    }
}
