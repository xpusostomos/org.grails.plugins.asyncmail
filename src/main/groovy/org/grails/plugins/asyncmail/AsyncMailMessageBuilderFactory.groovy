package org.grails.plugins.asyncmail

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import org.springframework.mail.javamail.JavaMailSender

import javax.activation.FileTypeMap
import javax.activation.MimetypesFileTypeMap
/**
 * Create a message builder.
 *
 * @author Vitalii Samolovskikh aka Kefir
 * @coauthor Puneet Behl
 */
class AsyncMailMessageBuilderFactory implements GrailsConfigurationAware {
    def mailMessageContentRenderer
    def mailSender
    Config configuration
    private final FileTypeMap fileTypeMap = new MimetypesFileTypeMap()

    AsyncMailMessageBuilder createBuilder() {
        AsyncMailMessageBuilder builder = new AsyncMailMessageBuilder(
                (mailSender instanceof JavaMailSender),
                configuration,
                fileTypeMap,
                mailMessageContentRenderer
        )
        builder.init(configuration)
        return builder
    }
}
