package org.grails.plugins.asyncmail

import grails.gorm.transactions.Transactional
import org.grails.plugins.asyncmail.enums.MessageStatus
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@Slf4j
@Transactional
class AsyncMailPersistenceService {

    AsyncMailConfigService asyncMailConfigService

    @CompileStatic
    AsyncMailMessage save(
            AsyncMailMessage message, boolean flush, boolean validate
    ) {
        return message.save(flush: flush, failOnError: true, validate: validate)
    }

    @CompileStatic
    void delete(AsyncMailMessage message, boolean flush) {
        message.delete(flush: flush)
    }

    @CompileStatic
    void deleteAttachments(AsyncMailMessage message, boolean flush) {
        message.attachments.clear()
        message.save(flush: flush)
    }

    @CompileStatic
    AsyncMailMessage getMessage(long id) {
        return AsyncMailMessage.get(id)
    }

    List<Long> selectMessagesIdsForSend() {
        return AsyncMailMessage.withCriteria {
            Date now = new Date()
            lt('beginDate', now)
            gt('endDate', now)
            ltProperty('attemptsCount', 'maxAttemptsCount')
            or {
                eq('status', MessageStatus.CREATED)
                eq('status', MessageStatus.ATTEMPTED)
            }
            order('priority', 'desc')
            order('endDate', 'asc')
            order('attemptsCount', 'asc')
            order('beginDate', 'asc')
            maxResults(asyncMailConfigService.messagesAtOnce)
            projections {
                if (asyncMailConfigService.mongo) {
                    id()
                } else {
                    property('id')
                }
            }
        } as List<Long>
    }

    void updateExpiredMessages() {
        int count = 0
        if (asyncMailConfigService.mongo) {
            AsyncMailMessage.withCriteria {
                lt "endDate", new Date()
                or {
                    eq "status", MessageStatus.CREATED
                    eq "status", MessageStatus.ATTEMPTED
                }
            }.each {
                it.status = MessageStatus.EXPIRED
                it.save(flush: true)
                count++
            }
        } else {
            // This could be done also with the above code.
            AsyncMailMessage.withTransaction {
                count = AsyncMailMessage.executeUpdate(
                        "update AsyncMailMessage amm set amm.status=:es where amm.endDate<:date and (amm.status=:cs or amm.status=:as)",
                        ["es": MessageStatus.EXPIRED, "date": new Date(), "cs": MessageStatus.CREATED, "as": MessageStatus.ATTEMPTED]
                )
            }
        }
        log.trace("${count} expired messages were updated.")
    }

    void flush() {
        AsyncMailMessage.withSession { session ->
            session.flush()
        }
    }
}
