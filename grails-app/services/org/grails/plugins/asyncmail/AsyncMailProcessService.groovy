package org.grails.plugins.asyncmail

import grails.async.Promise
import grails.async.Promises
import grails.core.GrailsApplication
import org.grails.plugins.asyncmail.enums.MessageStatus
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.mail.MailException
import org.springframework.mail.MailParseException
import org.springframework.mail.MailPreparationException

import javax.sql.DataSource
import java.sql.ResultSet
import java.util.concurrent.ArrayBlockingQueue
import grails.gorm.transactions.Transactional

import static grails.async.Promises.task

@Slf4j
@CompileStatic
//@Transactional
class AsyncMailProcessService {

    GrailsApplication grailsApplication

    AsyncMailPersistenceService asyncMailPersistenceService
    AsyncMailSendService asyncMailSendService
    AsyncMailConfigService asyncMailConfigService
    DataSource dataSource
    boolean tablesExist = false

    /**
     * Some apps like to manually drop and create all the tables in Bootstrap. We need to give it time to drop, then create
     * again. A bit of a fudge but grails doesn't give a precise way to do this, and in any case, there's no rush
     * to send out emails on startup. We should probably make this behavior a parameter so you can turn it off.
     */
    void waitForTablesToExist() {
        Object.sleep(2000)
        while (!tablesExist) {
            ResultSet resultSet = dataSource.getConnection().getMetaData().getTables(null, null, 'async_mail_mess', null)
            if (resultSet.next()) {
                tablesExist = true
            } else {
                log.warn('asyncmail: waiting for tables to exist')
                Object.sleep(2000)
            }
        }
    }

    void findAndSendEmails() {
        // Get messages from DB
        List<Long> messagesIds = asyncMailPersistenceService.selectMessagesIdsForSend()

        if (messagesIds) {
            log.debug("Found ${messagesIds} messages to send.")

            // Create the queue of ids for processing
            int messageCount = messagesIds.size()
            Queue<Long> idsQueue = new ArrayBlockingQueue<Long>(messageCount, false, messagesIds)

            // Create some parallel tasks
            List<Promise> promises = []
            int taskCount = Math.min(asyncMailConfigService.taskPoolSize, messageCount)

            log.debug("Starts $taskCount send tasks.")

            for (int i = 0; i < taskCount; i++) {
                promises << task {
                    AsyncMailMessage.withNewSession {
                        AsyncMailMessage.withTransaction {
                            Long messageId
                            while ((messageId = idsQueue.poll()) != null) {
                                try {
                                    processEmailMessage(messageId)
                                } catch (Exception e) {
                                    log.error(
                                            "An exception was thrown when attempt to send a message with id=${messageId}.",
                                            e
                                    )
                                }
                            }

                            if (!asyncMailConfigService.useFlushOnSave) {
                                asyncMailPersistenceService.flush()
                            }
                        }
                    }
                }
            }

            // To prevent concurrent job execution we have to wait for all tasks when they will send all messages
            Promises.waitAll(promises)
            log.debug("$taskCount tasks are completed.")
        } else {
            log.trace("Messages to send have not be found.")
        }
    }

    void processEmailMessage(long messageId) {
        boolean useFlushOnSave = asyncMailConfigService.useFlushOnSave

        AsyncMailMessage message = asyncMailPersistenceService.getMessage(messageId)

        if (!message) {
            log.error("Can't find message with id=${messageId}.")
            return
        }

        log.trace("Got a message: " + message.toString())

        Date now = new Date()
        Date attemptDate = new Date(now.getTime() - message.attemptInterval)
        boolean canAttempt = message.hasAttemptedStatus() && message.lastAttemptDate.before(attemptDate)
        if (message.hasCreatedStatus() || canAttempt) {
            message.lastAttemptDate = now
            message.attemptsCount++

            // Guarantee that e-mail can't be sent more than 1 time
            message.status = MessageStatus.ERROR
            asyncMailPersistenceService.save(message, useFlushOnSave, false)

            // Validate message
            if (!message.validate()) {
                message.errors.allErrors.each {
                    log.error(it as String)
                }
                return
            }

            // Attempt to send
            try {
                log.trace("An attempt to send the message with id=${message.id}.")
                asyncMailSendService.send(message)
                message.sentDate = now
                message.status = MessageStatus.SENT
                log.trace("The message with id=${message.id} was sent successfully.")
            } catch (MailException e) {
                log.warn("An attempt to send the message with id=${message.id} was failed.", e)
                canAttempt = message.attemptsCount < message.maxAttemptsCount
                boolean fatalException = e instanceof MailParseException || e instanceof MailPreparationException
                if (canAttempt && !fatalException) {
                    message.status = MessageStatus.ATTEMPTED
                }
            } finally {
                asyncMailPersistenceService.save(message, useFlushOnSave, false)
            }

            // Delete message if it is sent successfully and can be deleted
            if (message.hasSentStatus() && message.markDelete) {
                asyncMailPersistenceService.delete(message, useFlushOnSave)
                log.trace("The message with id=${messageId} was deleted.")
            } else if (message.hasSentStatus() && message.markDeleteAttachments) {
                asyncMailPersistenceService.deleteAttachments(message, useFlushOnSave)
                log.trace("The message with id=${messageId} had all its attachments deleted.")
            } else {
                log.trace("The message with id=${messageId} will not be deleted.")
            }
        }
    }
}
