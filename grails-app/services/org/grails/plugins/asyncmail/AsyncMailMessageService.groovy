package org.grails.plugins.asyncmail

import grails.gorm.services.Service

@Service(AsyncMailMessage)
interface AsyncMailMessageService {

    AsyncMailMessage get(Serializable id)

    List<AsyncMailMessage> list(Map args)

    List<AsyncMailMessage> findAll(Map args)

    Long count()

    void delete(Serializable id)

    AsyncMailMessage save(AsyncMailMessage AsyncMailMessage)

}
