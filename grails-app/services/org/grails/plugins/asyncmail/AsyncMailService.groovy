package org.grails.plugins.asyncmail

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.validation.ObjectError

@Slf4j
@CompileStatic
class AsyncMailService {
    AsyncMailPersistenceService asyncMailPersistenceService
    AsyncMailMessageBuilderFactory asyncMailMessageBuilderFactory
    AsyncMailConfigService asyncMailConfigService

    /**
     * Create async message and save it to the DB.
     *
     * If configuration flag async.mail.send.immediately is true (default)
     * then this method starts the send job after message is created
     */
    AsyncMailMessage sendAsyncMail(@DelegatesTo(AsyncMailMessageBuilder) Closure callable) {
        def messageBuilder = asyncMailMessageBuilderFactory.createBuilder()
        callable.delegate = messageBuilder
        callable.resolveStrategy = Closure.DELEGATE_FIRST
        callable.call()

        // Mail message
        AsyncMailMessage message = messageBuilder.message

        // Get immediately behavior configuration
        boolean immediately
        if (messageBuilder.immediatelySetted) {
            immediately = messageBuilder.immediately
        } else {
            immediately = asyncMailConfigService.sendImmediately
        }
        immediately =
            immediately &&
                    message.beginDate.time <= System.currentTimeMillis() &&
                    !asyncMailConfigService.disable

        // Save message to DB
		def savedMessage = null
		if(immediately && asyncMailConfigService.newSessionOnImmediateSend) {
            AsyncMailMessage.withNewSession {
                savedMessage = asyncMailPersistenceService.save(message, true, true)
            }
        } else {
            savedMessage = asyncMailPersistenceService.save(message, asyncMailConfigService.useFlushOnSave, true)
        }

        if (!savedMessage) {
            StringBuilder errorMessage = new StringBuilder()
            message.errors?.allErrors?.each {ObjectError error ->
                errorMessage.append(error.getDefaultMessage())
            }
            throw new Exception(errorMessage.toString())
        }

        // Start job immediately
        if (immediately) {
            log.trace("Start send job immediately.")
            sendImmediately()
        }

        return message
    }

    /**
     * @see #sendAsyncMail
     */
    AsyncMailMessage sendMail(@DelegatesTo(AsyncMailMessageBuilder) Closure callable) {
        return sendAsyncMail(callable)
    }

    /**
     * Start send job immediately. If you send more than one message in one method,
     * you can disable async.mail.send.immediately flag (default true) and use this method
     * after creating all messages.
     *
     * <code>asyncMailService.sendAsyncMail{...}<br/>
     * asyncMailService.sendAsyncMail{...}<br/>
     * asyncMailService.sendAsyncMail{...}<br/>
     * asyncMailService.sendImmediately()</code>
     */
    void sendImmediately() {
        AsyncMailJob.triggerNow()
    }
}
