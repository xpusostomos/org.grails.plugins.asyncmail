package org.grails.plugins.asyncmail

import groovy.util.logging.Slf4j

/**
 * Send asynchronous messages
 */
@Slf4j
class AsyncMailJob {
    static triggers = {}

    static concurrent = false
    static group = "AsyncMail"


    // Dependency injection
    AsyncMailProcessService asyncMailProcessService

    def execute() {
        log.trace('Entering execute method.')
        asyncMailProcessService.waitForTablesToExist()
        def startDate = System.currentTimeMillis()

        asyncMailProcessService.findAndSendEmails()

        def endDate = System.currentTimeMillis()
        log.trace("Exiting execute method. Execution time = ${endDate - startDate}ms")
    }
}
