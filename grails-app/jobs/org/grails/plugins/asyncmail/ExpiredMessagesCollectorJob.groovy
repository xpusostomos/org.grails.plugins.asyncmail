package org.grails.plugins.asyncmail

import groovy.util.logging.Slf4j

@Slf4j
class ExpiredMessagesCollectorJob {
    static triggers = {}

    static concurrent = false
    static group = "AsyncMail"

    AsyncMailPersistenceService asyncMailPersistenceService

    def execute() {
        log.trace('Entering execute method.')
        asyncMailPersistenceService.updateExpiredMessages()
        log.trace('Exiting execute method.')
    }
}
