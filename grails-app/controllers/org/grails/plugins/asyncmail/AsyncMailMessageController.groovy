package org.grails.plugins.asyncmail

import org.grails.plugins.asyncmail.AsyncMailMessage
import org.grails.plugins.asyncmail.enums.MessageStatus

class AsyncMailMessageController {
//    static scaffold = AsyncMailMessage
    AsyncMailMessageService asyncMailMessageService
    static defaultAction = 'index'

    static allowedMethods = [update: 'POST']

    /**
     * List messages.
     */
    def index() {
        params.max = Math.min(params.max ? params.max.toInteger() : 10, 100)
        params.sort = params.sort ?: 'dateCreated'
        params.order = params.order ?: 'desc'
        [resultList: AsyncMailMessage.list(params)]
    }

    def findAllByTo(String id) {
        def results = AsyncMailTo.where {
            toString == id
        }.list()
        render view: 'index', model: [resultList: results.collect { it.message }]
    }

    def findAllByToIlike(String id) {
        def results = AsyncMailTo.where {
            ilike 'toString', "%$id%"
        }.list()
        render view: 'index', model: [resultList: results.collect { it.message }]
    }

    private withMessage(Closure cl) {
        AsyncMailMessage message = AsyncMailMessage.get(params.id)
        if (message) {
            return cl(message)
        }

        flash.message = "The message ${params.id} was not found."
        flash.error = true
        redirect(action: 'index')
    }

    /**
     * Show message data.
     */
    def show() {
//        render controller: 'asyncMail', view: 'show', model: [ message: new AsyncMailMessage() ]
        withMessage {AsyncMailMessage message ->
            return [message: message]
        }
    }

    /**
     * Edit message data.
     */
    def edit() {
        withMessage {AsyncMailMessage message ->
            return [message: message]
        }
    }

    /**
     * Update message.
     */
    def update() {
        withMessage {AsyncMailMessage message ->
            bindData(
                    message, params,
                    [include:
                            [
                                    'status',
                                    'beginDate',
                                    'endDate',
                                    'maxAttemptsCount',
                                    'attemptInterval',
                                    'priority',
                                    'markDelete'
                            ]
                    ]
            )
            message.attemptsCount = 0
            if (!message.hasErrors() && message.save(flush: true)) {
                flash.message = "The message ${params.id} was updated."
                redirect(action: 'show', id: message.id)
            } else {
                render(view: 'edit', model: [message: message])
            }
        }
    }

    /**
     * Abort message sending.
     */
    def abort() {
        withMessage {AsyncMailMessage message ->
            if (message.abortable) {
                message.status = MessageStatus.ABORT
                if (message.save(flush: true)) {
                    flash.message = "The message ${message.id} was aborted."
                } else {
                    flash.message = "Can't abort the message ${message.id}."
                    flash.error = true
                }
            } else {
                flash.message = "Can't abort the message ${message.id} with the status ${message.status}."
                flash.error = true
            }
            redirect(action: 'index')
        }
    }

    /**
     * Delete message.
     */
    def delete() {
        withMessage {AsyncMailMessage message ->
            try {
                asyncMailMessageService.delete(message)
//                message.delete(flush: true)
                flash.message = "The message ${message.id} was deleted."
                redirect(action: 'index')
            } catch (Exception e) {
                def errorMessage = "Can't delete the message with the id ${message.id}.";
                log.error(errorMessage, e)
                flash.message = errorMessage
                flash.error = true
                redirect(action: 'show', id: message.id)
            }
        }
    }
}
