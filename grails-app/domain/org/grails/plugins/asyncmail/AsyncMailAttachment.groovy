package org.grails.plugins.asyncmail

class AsyncMailAttachment implements Serializable {

    static final DEFAULT_MIME_TYPE = 'application/octet-stream'

    private static final SIZE_30_MB = 30*1024*1024

    String attachmentName
    String mimeType = DEFAULT_MIME_TYPE
    byte[] content
//    java.sql.Blob content
    boolean inline = false

    static belongsTo = [message:AsyncMailMessage]

    static mapping = {
        table 'async_mail_attachment'
        version false
//        content sqlType: 'blob'
//        content type: 'blob'
    }

    static constraints = {
        attachmentName(blank:false)
        //mimeType()
        content(maxSize:SIZE_30_MB)
    }
}
