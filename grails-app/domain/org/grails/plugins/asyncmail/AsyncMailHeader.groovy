package org.grails.plugins.asyncmail

import org.apache.commons.lang3.builder.HashCodeBuilder

class AsyncMailHeader implements Serializable {
    int messageId
    int headerName
    String headerValue

    static transients = ['message']

    static constraints = {
    }

    static mapping = {
        version false
        id composite: [ 'messageId', 'headerName']
    }

    AsyncMailMessage getMessage() {
        return AsyncMailMessage.get(messageId)
    }
    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append(messageId.hashCode())
        builder.append(headerName.hashCode())
        builder.toHashCode()
    }

    boolean equals(other) {
        if (!(other instanceof AsyncMailHeader)) {
            return false
        }
        other.messageId == messageId && other.headerName == headerName
    }
}
