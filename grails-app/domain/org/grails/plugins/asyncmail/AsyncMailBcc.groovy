package org.grails.plugins.asyncmail

import org.apache.commons.lang3.builder.HashCodeBuilder

class AsyncMailBcc implements Serializable {
    int messageId
    int bccIdx
    String bccString

    static transients = ['message']

    static constraints = {
    }

    static mapping = {
        version false
        id composite: [ 'messageId', 'bccIdx']
    }

    AsyncMailMessage getMessage() {
        return AsyncMailMessage.get(messageId)
    }
    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append(messageId.hashCode())
        builder.append(bccIdx.hashCode())
        builder.toHashCode()
    }

    boolean equals(other) {
        if (!(other instanceof AsyncMailBcc)) {
            return false
        }
        other.messageId == messageId && other.bccIdx == bccIdx
    }
}
