package org.grails.plugins.asyncmail

import org.apache.commons.lang3.builder.HashCodeBuilder

class AsyncMailTo implements Serializable {
    int messageId
    int toIdx
    String toString

    static transients = ['message']

    static constraints = {
    }

    static mapping = {
        version false
        id composite: [ 'messageId', 'toIdx']
    }

    AsyncMailMessage getMessage() {
        return AsyncMailMessage.get(messageId)
    }
    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append(messageId.hashCode())
        builder.append(toIdx.hashCode())
        builder.toHashCode()
    }

    boolean equals(other) {
        if (!(other instanceof AsyncMailTo)) {
            return false
        }
        other.messageId == messageId && other.toIdx == toIdx
    }
}
