package org.grails.plugins.asyncmail

import org.apache.commons.lang3.builder.HashCodeBuilder

class AsyncMailCc implements Serializable {
    int messageId
    int ccIdx
    String ccString

    static transients = ['message']

    static constraints = {
    }

    static mapping = {
        version false
        id composite: [ 'messageId', 'ccIdx']
    }

    AsyncMailMessage getMessage() {
        return AsyncMailMessage.get(messageId)
    }
    int hashCode() {
        def builder = new HashCodeBuilder()
        builder.append(messageId.hashCode())
        builder.append(ccIdx.hashCode())
        builder.toHashCode()
    }

    boolean equals(other) {
        if (!(other instanceof AsyncMailCc)) {
            return false
        }
        other.messageId == messageId && other.ccIdx == ccIdx
    }
}
